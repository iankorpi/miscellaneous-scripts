import sys
import os
import subprocess

from PIL import Image
from os import path

if len(sys.argv) != 4: # wrong number of arguments
    print("usage: python sheetconv.py <path to spritesheet> <num of rows> <num of columns>")
    exit()

filename = sys.argv[1] # first argument

if not path.exists(filename):
    print("That file does not exist. Specify the path to a spritesheet.")
    exit()

try:
    int(sys.argv[2])
except:
    print("Invalid argument for number of rows in the spritesheet. Enter an integer.")
    exit()

try:
    int(sys.argv[3])
except:
    print("Invalid argument for number of columns in the spritesheet. Enter an integer.")
    exit()

print("Splitting spritesheet '{}'...".format(filename))

fnames = filename.split('.') # split according to period in filename - this isn't very sanitary in some cases e.g. hidden folders

label = fnames[0]

im = Image.open(filename) # open file in read mode
ss_w, ss_h = im.size # dimensions of original spritesheet

columns = int(sys.argv[3]) # number of columns
rows = int(sys.argv[2]) # number of rows

# calculate dimensions of individual sprites
sprite_w = int(ss_w / columns)
sprite_h = int(ss_h / rows)

n = 0 # sprite number
for row in range(0, rows):
    for col in range(0, columns):
        # get position of sprite
        l = col*sprite_w
        t = row*sprite_h
        r = l+sprite_w
        b = t+sprite_h
        sprite = im.crop((l, t, r, b)) # crop based on position

        name = label + str(n) + ".png"
        sprite.save(name, "PNG")
        n += 1 # increment sprite number

num_frames = rows * columns

proc = [
'ffmpeg',
 '-r',
 str(num_frames),
 '-f',
 'image2',
 '-s',
 'x'.join((str(sprite_w), str(sprite_h))),
 '-start_number',
 '0',
 '-i',
 (label+'%d.png'),
 '-vframes',
 str(num_frames),
 '-vcodec',
 'libx264',
 (label+'.mp4')]

subprocess.call(proc)
